from django.contrib.auth.models import User, Group

from django.db import models
from django.utils.translation import ugettext_lazy as _

from datetime import datetime


class Template(models.Model):
   '''E-mail template'''

   class Meta:
      verbose_name = _(u'template')

   @staticmethod
   def autocomplete_search_fields():
      return ('subject__icontains',)

   subject = models.CharField(_(u'subject'), max_length=998)
   body = models.TextField(_(u'body'))

   def __unicode__(self):
      return self.subject


class _U(object):

   def __unicode__(self):
      return self.name


class CampaignStatus(models.Model, _U):

   _none, new, active, finished, scheduled = xrange(5)

   choices = (
      (new, _(u'new')),
      (active, _(u'active')),
      (finished, _(u'finished')),
      (scheduled, _(u'scheduled')),
   )

   id = models.IntegerField(primary_key=True, choices=choices)
   name = models.CharField(_(u'name'), max_length=24)


class Tag(models.Model, _U):

   _none, test, campaign = xrange(3)

   choices = (
      (test, 'test'),
      (campaign, 'campaign'),
   )

   id = models.IntegerField(primary_key=True, default=campaign)
   name = models.CharField(_(u'name'), max_length=100)


class AbstractCampaign(models.Model):

   class Meta:
      abstract = True

   tag = models.ForeignKey(
      Tag,
      verbose_name=_(u'tag'),
      default=Tag.campaign,
      related_name='%(app_label)s_campaigns'
   )

   created = models.DateTimeField(_(u'created'), auto_now_add=True)
   started = models.DateTimeField(_(u'started'), null=True, blank=True)
   finished = models.DateTimeField(_(u'finished'), null=True, blank=True)
   scheduled = models.DateTimeField(_(u'scheduled'), null=True, blank=True)
   status = models.ForeignKey(
      CampaignStatus,
      verbose_name=_(u'status'),
      default=CampaignStatus.new,
      related_name='%(app_label)s_campaigns'
   )

   subject = models.CharField(_(u'subject'), max_length=998, null=True, blank=True)

   from_email = models.EmailField(_(u'from email'), max_length=254)
   from_name = models.CharField(_(u'from name'), max_length=1000)

   sent = models.IntegerField(_(u'sent'), default=0)
   opens = models.IntegerField(_(u'opens'), default=0)
   clicks = models.IntegerField(_(u'clicks'), default=0)
   unsubscribes = models.IntegerField(_(u'unsubscribes'), default=0)


class Campaign(AbstractCampaign):
   '''Mass mail campaign'''

   class Meta:
      verbose_name = _(u'campaign')

   name = models.CharField(_(u'name'), max_length=2000)

   template = models.ForeignKey(Template, verbose_name=_(u'template'))

   users = models.ManyToManyField(User, verbose_name=_(u'users'), blank=True)
   groups = models.ManyToManyField(Group, verbose_name=_(u'groups'), blank=True)

   def save(self, *args, **kwargs):
      if not self.subject:
         self.subject = self.template.subject

      super(Campaign, self).save(*args, **kwargs)

   def __unicode__(self):
      return self.name


class Direction(models.Model, _U):

   class Meta:
      ordering = ['id']

   none, outgoing, incoming = xrange(3)

   choices = (
      (outgoing, _(u'outgoing')),
      (incoming, _(u'incoming')),
   )

   id = models.IntegerField(primary_key=True, choices=choices)
   name = models.CharField(_(u'name'), max_length=24)


class Medium(models.Model, _U):

   class Meta:
      ordering = ['id']
      verbose_name_plural = _(u'medium')

   none, email, phone, sms, skype, tw, fb, li = xrange(8)

   name = models.CharField(_(u'name'), max_length=50)


class MessageStatus(models.Model, _U):

   class Meta:
      ordering = ['id']
      verbose_name_plural = _(u'message statuses')

   none, unknown, delivered, bounce, soft_bounce, unsub, reject, spam = xrange(8)

   name = models.CharField(_(u'name'), max_length=50)


class AbstractCommunication(models.Model):

   class Meta:
      abstract = True

   user = models.ForeignKey(
      User,
      verbose_name=_(u'user'),
      related_name='%(app_label)s_communications'
   )

   status = models.ForeignKey(
      MessageStatus,
      default=MessageStatus.unknown,
      related_name='%(app_label)s_communications'
   )
   dt = models.DateTimeField(_(u'event date'), default=datetime.now)

   opens = models.IntegerField(_(u'opens'), default=0)
   clicks = models.IntegerField(_(u'clicks'), default=0)


class Communication(AbstractCommunication):

   campaign = models.ForeignKey(
      Campaign,
      null=True, blank=True,
      related_name='%(app_label)s_communications'
   )

   direction = models.ForeignKey(Direction, default=Direction.outgoing)
   medium = models.ForeignKey(Medium, default=Medium.email)

   template = models.ForeignKey(Template, null=True, blank=True)
   comment = models.TextField(_(u'comment'), null=True, blank=True)

   _m = ('', u'To', u'From')

   def __unicode__(self):
      return '%s %s' % (self._m[self.direction.pk], self.user.email)


class IpAddress(models.Model, _U):

   name = models.CharField(_(u'name'), max_length=39)


class UserAgent(models.Model, _U):

   name = models.CharField(_(u'user agent'), max_length=2000)


class Location(models.Model, _U):

   name = models.CharField(_(u'location'), max_length=2000)


class Url(models.Model, _U):

   name = models.CharField(_(u'url'), max_length=2000) # 2083 max


event_foreign_models = IpAddress, UserAgent, Location, Url


class AbstractEvent(models.Model):

   class Meta:
      abstract = True

   user = models.ForeignKey(User,
      verbose_name=_(u'user'),
      related_name='%(app_label)s_events'
   )

   dt = models.DateTimeField(_(u'event date'))

   ip = models.ForeignKey(IpAddress, verbose_name=_(u'ip address'), null=True, blank=True)
   ua = models.ForeignKey(UserAgent, verbose_name=_(u'user agent'), null=True, blank=True)
   location = models.ForeignKey(Location, verbose_name=_(u'location'), null=True, blank=True)
   url = models.ForeignKey(Url, verbose_name=_(u'click url'), null=True, blank=True)

   opens = models.IntegerField(_(u'opens'), default=0)
   clicks = models.IntegerField(_(u'clicks'), default=0)


