from django.views.generic import TemplateView


class UnsubscribeView(TemplateView):

   template_name = 'fmail/unsubscribe.html'


