from .models import (
   CampaignStatus, Communication, MessageStatus, Direction,
   AbstractEvent, IpAddress, UserAgent, Location, Url
)

import mandrill

from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.template import Context
from django.template.loader import render_to_string
from django.utils.timezone import localtime
from django.db.models import Sum, F
from django.db import transaction

import pytz
from datetime import datetime, timedelta
import time
from base64 import b64encode
from itertools import chain
import sys


class Tag(object):

   test, campaign = 'test', 'campaign'


class Mailer(object):

   def __init__(self, req, campaign, users_expr):
      self._site = '%s://%s' % (req.scheme, req.get_host())
      self._camp = campaign
      self._users = list(users_expr)

   def send(self, tags=[Tag.test]):
      camp = self._camp

      msg = EmailMessage(
         subject=camp.subject,
         body=self._render_mail(),
         from_email='"%s <%s>"' % (camp.from_name, camp.from_email),
         to=[ x.email for x in self._users ],
      )
      msg.content_subtype = 'html'
      msg.track_clicks = False

      msg.merge_vars = {
         x.email : {
            'first_name': x.first_name,
            'eid': b64encode(x.email),
         } for x in self._users
      }

      msg.tags = tags
      msg.metadata = dict(campaign=camp.pk)

      msg.send()

   def _render_mail(self):
      ctx = {
         'site': self._site,
         'campaign': self._camp,
      }
      return render_to_string('fmail/email.html', Context(ctx))


class Segmentor(object):

   _pred = dict(is_active=True, email__isnull=False)

   @classmethod
   def campaign_users(cls, campaign):
      gusers = User.objects.filter(
         groups__in=[ g.pk for g in campaign.groups.all() ],
         **cls._pred
      )
      users = campaign.users.filter(**cls._pred)

      return gusers | users


def get_user(email):
   qs = User.objects.filter(email=email)

   return qs[0] if qs else User(first_name='Man', email=email)

def test_campaign(req, camp, emails):
   if not emails:
      return

   mailer = Mailer(req, camp, ( get_user(x) for x in emails ))
   mailer.send()

def run_campaign(req, camp):
   if camp.status.pk != CampaignStatus.new:
      raise RuntimeError('campaign "%s" was started already' % camp.name)

   mailer = Mailer(req, camp, Segmentor.campaign_users(camp))
   mailer.send(tags=[Tag.campaign])

   camp.status = CampaignStatus(pk=CampaignStatus.active)
   camp.started = datetime.now()
   camp.save()


class _NullEvent(AbstractEvent):

   class Meta:
      abstract = True

   class _Manager(object):

      def filter(self, *args, **kwargs):
         return self

      def bulk_create(self, *args, **kwargs):
         pass

      def delete(self, *args, **kwargs):
         pass

      def make_objects(self, *args, **kwargs):
         return []

   objects = _Manager()


class Factory(object):

   communication_class = None
   event_class = None

   @classmethod
   def campaign_users(cls, campaign):
      raise NotImplementedError()

   @classmethod
   def communication_filter(cls):
      return {}

   def __init__(self, create_events_method):
      ec = self.event_class

      if ec is None or ec._meta.abstract:
         self.event_class = _NullEvent
      else:
         self.event_class.objects.make_objects = create_events_method


class DefaultFactory(Factory):

   communication_class = Communication
   event_class = AbstractEvent

   @classmethod
   def campaign_users(cls, campaign):
      return Segmentor.campaign_users(campaign)

   @classmethod
   def communication_filter(cls):
      return dict(direction=Direction.outgoing)


class Cache(object):
   _data = {Location:{}, IpAddress: {}, Url: {}, UserAgent: {}}

   def data(self, model, name):
      if name in self._data[model]:
         return self._data[model][name]
      obj, created = model.objects.get_or_create(name=name)
      return obj

   def location(self, name):
      return self.data(Location, name)

   def ip(self, name):
      return self.data(IpAddress, name)

   def ua(self, name):
      return self.data(UserAgent, name)

   def url(self, name):
      return self.data(Url, name)


class MailData(object):

   _api_count_limit = 1000

   factory = DefaultFactory

   @classmethod
   def _enorm(cls, email):
      '''normalize email'''
      return email.lower().strip()

   def __init__(self, mandrill_key, campaign, timezone='UTC'):
      f = self.factory(self._make_events)
      self._Comm = f.communication_class
      self._Event = f.event_class

      self._client = mandrill.Mandrill(mandrill_key)
      self._camp = campaign
      self._cache = Cache()
      self._tz = pytz.timezone(timezone)

   def collect(self, filter_by_ts=None):
      self._users = {
         self._enorm(x.email) : x for x in self.factory.campaign_users(self._camp)
      }
      filter_args = self._make_search_filter(filter_by_ts)

      if self._Comm.objects.filter(campaign=self._camp).exists():
         upsert = self._upsert
         _aggr = lambda x: None
      else:
         upsert = self._insert
         def _aggr(d):
            self._camp.sent = F('sent') + len(d)
            self._camp.save()

      for data in self._iter_search(**filter_args):
         upsert(data)
         _aggr(data)

      self._aggregate()

   def _iter_search(self, **filter_args):
      if len(self._users) < self._api_count_limit:
         yield self._search(**filter_args)
         return

      chunk, next_ts = self._find_series(**filter_args)
      yield chunk

      minute = 60
      step = minute
      zz = 0 # zeroes
      while True:
         last = next_ts + step
         chunk = self._search_ts(next_ts + 1, last, **filter_args)

         size = len(chunk)
         zz *= int(size == 0)

         #print '[%d, %d], %d' % (next_ts + 1, next_ts + step, size)

         if size == self._api_count_limit:
            if step == 1:
               raise RuntimeError('more than 1K results in 1 second')
            step /= 2
            continue
         elif size == 0:
            if zz > 4:
               break

            zz += 1
            step = min(2*step, 2*minute)
         elif size < self._api_count_limit*0.5:
            step = minute

         next_ts = last

         yield chunk

   def _find_series(self, **filter_args):
      step = 3*60
      start = time.mktime(localtime(self._camp.started).timetuple())

      ts = start
      next_ts = start + step

      while True:
         chunk = self._search_ts(ts, next_ts, **filter_args)
         size = len(chunk)
         if size == 0:
            ts += step
            next_ts += step
         elif size == self._api_count_limit:
            step /= 2
            next_ts = ts + step
         else:
            #print 'found %d at [%d, %d] (begin: %d)' % (size, ts, next_ts, start)
            #print len(self._search_ts(start, next_ts, **filter_args))
            break

      return chunk, next_ts

   def _search(self, **args):
      try:
         return self._client.messages.search(limit=self._api_count_limit, **args)
      except mandrill.ServiceUnavailableError, e:
         #print e
         #print 'sleeping 60 seconds'
         time.sleep(60)
         return self._search(**args)

   def _search_ts(self, ts_from, ts_to, **args):
      d = dict(args) if args.get('query') else args
      q = d.pop('query', None)

      tsf = 'ts:[%d TO %d]' % (ts_from, ts_to)
      q = ' AND '.join((q, tsf)) if q else tsf

      return self._search(query=q, **d)

   def _make_search_filter(self, filter_by_ts):
      def _fmt(dt):
         return dt.strftime('%Y-%m-%d')

      if filter_by_ts:
         d = datetime.strptime(filter_by_ts, '%Y-%m-%d %H:%M:%S')

         #gmt = time.gmtime(time.mktime(d.timetuple()))
         #ts = time.mktime(gmt)
         ts = time.mktime(d.timetuple())

         #TODO: pass offset
         query = 'ts:[%d TO %d]' % (ts, ts + 60*60) # + 1 hour
         ds = _fmt(d)

         return dict(date_from=ds, date_to=ds, query=query, senders=[self._camp.from_email])
      else:
         d = _fmt(self._camp.started or self._camp.created)
         query = u'u_campaign:%d AND tags:%s' % (self._camp.pk, self._camp.tag.name)

         return dict(date_from=d, date_to=d, query=query)

   def _insert(self, data):
      pairs = filter(
         lambda pair: pair[0].user.pk, (self._communication_data(x) for x in data)
      )
      with transaction.atomic():
         self._Comm.objects.bulk_create(x[0] for x in pairs)
         self._Event.objects.bulk_create(
           chain(*(x[1] for x in pairs if x[1]))
         )

   def _upsert(self, data):
      with transaction.atomic():
         events = []
         pks = []

         for d in data:
            comm = self._communication(d)
            self._update(comm, d)
            events.extend(self._Event.objects.make_objects(comm, d))
            if comm.user.pk:
               pks.append(comm.user.pk)

         self._Event.objects.filter(campaign=self._camp, user__pk__in=pks).delete()
         self._Event.objects.bulk_create(events)

   def _update(self, comm, data):
      if not comm.user.pk:
         #TODO: logging
         #sys.stderr.write('email %s not found\n' % comm.user.email)
         return
      qs = self._Comm.objects.filter(
         campaign=self._camp,
         user=comm.user,
      ).filter(**self.factory.communication_filter()).order_by('dt')

      if not qs:
         #TODO: logging, exception?
         comm.save()
         return

      obj = qs.first()
      o = data['opens']
      c = data['clicks']
      if o + c == obj.opens + obj.clicks:
         return

      obj.opens = o
      obj.clicks = c
      obj.save()

   def _aggregate(self):
      qs = self._Comm.objects.filter(
         campaign=self._camp,
         status=MessageStatus.delivered
      )
      index = qs.aggregate(Sum('opens'), Sum('clicks'))

      self._camp.sent = qs.count()
      self._camp.opens = index['opens__sum'] or 0
      self._camp.clicks = index['clicks__sum'] or 0

      self._camp.save()

   def _communication(self, data):
      '''make communication object from json data'''
      user = self._get_user(data['email'])
      #if not user.pk:
      #   print >>sys.stderr, '%s not found' % user.email
      return self._Comm(
         user=self._get_user(data['email']),
         campaign=self._camp,
         status=MessageStatus(pk=self._status(data['state'])),
         dt=self._date_from_ts(self._event_ts(data)),
         opens=data['opens'],
         clicks=data['clicks']
      )

   def _communication_data(self, data):
      comm = self._communication(data)

      return comm, self._Event.objects.make_objects(comm, data)

   def _events_impl(self, data, **kwargs):
      events = []
      for x in (data or []):
         event = self._Event(dt=self._date_from_ts(x['ts']), **kwargs)

         for i in ('ip', 'ua', 'url', 'location'):
            value = x.get(i, None)
            if value is not None:
               event.__setattr__(i, self._cache.__getattribute__(i)(value))
         events.append(event)
      return events

   def _make_events(self, comm, data):
      if not comm.user.pk:
         return []

      args = dict(user=comm.user, campaign=comm.campaign)
      opens = self._events_impl(data.get('opens_detail'), opens=1, **args)
      clicks = self._events_impl(data.get('clicks_detail'), clicks=1, **args)

      return opens + clicks

   def _get_user(self, email):
      '''get campaign user or new user'''
      u = self._users.get(self._enorm(email))
      if u:
         return u

      #if users set is calculated on every run, user might not belong to the target group now
      #... let's make a database hit
      email = self._enorm(email)
      qs = User.objects.filter(email__icontains=email)
      if not qs:
         #print 'user %s not found' % email
         #TODO: self._log('user not found')
         return User(email=email)
      else:
         #print 'found user %s' % email
         #TODO: self._log('user was excluded from target group')
         user = qs[0]
         user.email = email
         self._users[email] = user
         return user

   _mandrill_states = {
      'sent': MessageStatus.delivered,
      'soft-bounced': MessageStatus.soft_bounce,
      'bounced': MessageStatus.bounce,
      'rejected': MessageStatus.reject,
      'spam': MessageStatus.spam,
   }

   @classmethod
   def _status(cls, state):
      return cls._mandrill_states.get(state, MessageStatus.unknown)

   def _date_from_ts(self, ts):
      dt = datetime.utcfromtimestamp(ts).replace(tzinfo=pytz.utc)
      return self._tz.normalize(dt.astimezone(self._tz))

   def _event_ts(self, data):
      ts = data['ts']
      events = data.get('smtp_events')

      if not events:
         return ts

      sent_event = next((x for x in events if x['type'] == 'sent'), None)

      return sent_event['ts'] if sent_event else ts


