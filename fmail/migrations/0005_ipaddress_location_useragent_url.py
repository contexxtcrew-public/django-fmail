# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import fmail.models


class Migration(migrations.Migration):

    dependencies = [
        ('fmail', '0004_auto_20151123_1700'),
    ]

    operations = [
        migrations.CreateModel(
            name='IpAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=39, verbose_name='name')),
            ],
            options={
            },
            bases=(models.Model, fmail.models._U),
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=2000, verbose_name='location')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserAgent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=2000, verbose_name='user agent')),
            ],
            options={
            },
            bases=(models.Model, fmail.models._U),
        ),
        migrations.CreateModel(
            name='Url',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=2000, verbose_name='url')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
