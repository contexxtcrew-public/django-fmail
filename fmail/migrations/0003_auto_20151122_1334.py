# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import fmail
from django_migration_fixture import fixture
from django.db import migrations, models
import fmail.models


class Migration(migrations.Migration):

    dependencies = [
        ('fmail', '0002_auto_20151012_1642'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.IntegerField(default=2, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name')),
            ],
            bases=(models.Model, fmail.models._U),
        ),
        migrations.RunPython(**fixture(fmail, 'tag.json')),
        migrations.AddField(
            model_name='campaign',
            name='clicks',
            field=models.IntegerField(default=0, verbose_name='clicks'),
        ),
        migrations.AddField(
            model_name='campaign',
            name='opens',
            field=models.IntegerField(default=0, verbose_name='opens'),
        ),
        migrations.AddField(
            model_name='campaign',
            name='sent',
            field=models.IntegerField(default=0, verbose_name='sent'),
        ),
        migrations.AlterField(
            model_name='campaign',
            name='status',
            field=models.ForeignKey(related_name='fmail_campaigns', default=1, verbose_name='status', to='fmail.CampaignStatus'),
        ),
        migrations.AddField(
            model_name='campaign',
            name='tag',
            field=models.ForeignKey(related_name='fmail_campaigns', default=2, verbose_name='tag', to='fmail.Tag'),
        ),
    ]
