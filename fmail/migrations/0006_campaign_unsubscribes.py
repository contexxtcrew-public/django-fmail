# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fmail', '0005_ipaddress_location_useragent_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='campaign',
            name='unsubscribes',
            field=models.IntegerField(default=0, verbose_name='unsubscribes'),
        ),
    ]
