# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import fmail
from django_migration_fixture import fixture
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fmail', '0006_campaign_unsubscribes'),
    ]

    operations = [
        migrations.AddField(
            model_name='campaign',
            name='scheduled',
            field=models.DateTimeField(null=True, verbose_name='scheduled', blank=True),
        ),
        migrations.AlterField(
            model_name='campaignstatus',
            name='id',
            field=models.IntegerField(serialize=False, primary_key=True, choices=[(1, 'new'), (2, 'active'), (3, 'finished'), (4, 'scheduled')]),
        ),
        migrations.RunPython(**fixture(fmail, 'campaign-status-scheduled.json')),
    ]
