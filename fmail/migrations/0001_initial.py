# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import fmail.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth', '__latest__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Campaign',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=2000, verbose_name='name')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='created')),
                ('subject', models.CharField(max_length=998, null=True, verbose_name='subject', blank=True)),
                ('from_email', models.EmailField(max_length=254, verbose_name='from email')),
                ('from_name', models.CharField(max_length=1000, verbose_name='from name')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name='groups', blank=True)),
            ],
            options={
                'verbose_name': 'campaign',
            },
        ),
        migrations.CreateModel(
            name='Communication',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dt', models.DateTimeField(default=datetime.datetime.now, verbose_name='event date')),
                ('comment', models.TextField(null=True, verbose_name='comment', blank=True)),
                ('opens', models.IntegerField(default=0, verbose_name='opens')),
                ('clicks', models.IntegerField(default=0, verbose_name='clicks')),
                ('campaign', models.ForeignKey(blank=True, to='fmail.Campaign', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Direction',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True, choices=[(1, 'outgoing'), (2, 'incoming')])),
                ('name', models.CharField(max_length=24, verbose_name='name')),
            ],
            options={
                'ordering': ['id'],
            },
            bases=(models.Model, fmail.models._U),
        ),
        migrations.CreateModel(
            name='Medium',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='name')),
            ],
            options={
                'ordering': ['id'],
                'verbose_name_plural': 'medium',
            },
            bases=(models.Model, fmail.models._U),
        ),
        migrations.CreateModel(
            name='MessageStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='name')),
            ],
            options={
                'ordering': ['id'],
                'verbose_name_plural': 'message statuses',
            },
            bases=(models.Model, fmail.models._U),
        ),
        migrations.CreateModel(
            name='Template',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(max_length=998, verbose_name='subject')),
                ('body', models.TextField(verbose_name='body')),
            ],
            options={
                'verbose_name': 'template',
            },
        ),
        migrations.AddField(
            model_name='communication',
            name='direction',
            field=models.ForeignKey(default=1, to='fmail.Direction'),
        ),
        migrations.AddField(
            model_name='communication',
            name='medium',
            field=models.ForeignKey(default=1, to='fmail.Medium'),
        ),
        migrations.AddField(
            model_name='communication',
            name='status',
            field=models.ForeignKey(default=1, to='fmail.MessageStatus'),
        ),
        migrations.AddField(
            model_name='communication',
            name='template',
            field=models.ForeignKey(blank=True, to='fmail.Template', null=True),
        ),
        migrations.AddField(
            model_name='communication',
            name='user',
            field=models.ForeignKey(verbose_name='user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='campaign',
            name='template',
            field=models.ForeignKey(verbose_name='template', to='fmail.Template'),
        ),
        migrations.AddField(
            model_name='campaign',
            name='users',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='users', blank=True),
        ),
    ]
