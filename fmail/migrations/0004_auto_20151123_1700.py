# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('fmail', '0003_auto_20151122_1334'),
    ]

    operations = [
        migrations.AlterField(
            model_name='communication',
            name='campaign',
            field=models.ForeignKey(related_name='fmail_communications', blank=True, to='fmail.Campaign', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='communication',
            name='status',
            field=models.ForeignKey(related_name='fmail_communications', default=1, to='fmail.MessageStatus'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='communication',
            name='user',
            field=models.ForeignKey(related_name='fmail_communications', verbose_name='user', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]
