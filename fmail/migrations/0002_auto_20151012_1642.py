# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import fmail
from django_migration_fixture import fixture
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fmail', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CampaignStatus',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True, choices=[(1, 'new'), (2, 'active'), (3, 'finished')])),
                ('name', models.CharField(max_length=24, verbose_name='name')),
            ],
        ),
        migrations.RunPython(**fixture(fmail, 'campaign-status.json')),
        migrations.AddField(
            model_name='campaign',
            name='finished',
            field=models.DateTimeField(null=True, verbose_name='finished', blank=True),
        ),
        migrations.AddField(
            model_name='campaign',
            name='started',
            field=models.DateTimeField(null=True, verbose_name='started', blank=True),
        ),
        migrations.AddField(
            model_name='campaign',
            name='status',
            field=models.ForeignKey(default=1, verbose_name='status', to='fmail.CampaignStatus'),
        ),
    ]
