from fmail.services import test_campaign, run_campaign
from fmail.models import (
   Template, Campaign,
   Direction, Medium, MessageStatus,
   Communication,
   event_foreign_models,
)

from django.template.response import TemplateResponse
from django.contrib import admin
from django.contrib import messages


class CampaignAdmin(admin.ModelAdmin):

   date_hierarchy = 'created'
   list_display = (
      'id', 'name', 'subject', 'from_email',
      'created', 'started', 'finished', 'status',
      'sent', 'opens', 'clicks', 'unsubscribes',
   )
   list_filter = ('status', )
   search_fields = ('name', 'subject',)

   filter_horizontal = ('users', 'groups',)

   raw_id_fields = ('template',)
   autocomplete_lookup_fields = {
      'fk': ('template',),
   }

   def test(self, req, campaigns):
      if req.POST.get('form'):
         emails = [ x.strip() for x in req.POST.get('emails').split(',') ]
         for c in campaigns:
            test_campaign(req, c, emails)
         self.message_user(req, 'Sample email was sent to %s' % ', '.join(emails))
      else:
         resp = TemplateResponse(
            req,
            'fmail/admin/test-campaign.html',
            {
               'email': req.user.email,
               'queryset': campaigns,
            },
         )
         return resp

   test.short_description = 'Test campaign'

   def run(self, req, campaigns):
      if req.POST.get('form'):
         try:
            for c in campaigns:
               run_campaign(req, c)
            self.message_user(req, 'Campaign %s started' % campaigns[0])
         except Exception, e:
            messages.error(req, e)
      else:
         resp = TemplateResponse(
            req,
            'fmail/admin/run-campaign.html',
            {
               'title': 'Are you sure you want to run campaign?',
               'queryset': campaigns,
            },
         )
         return resp

   run.short_description = 'Run campaign'

   actions = ('test', 'run', )


try:
   _campaign_filter = ('campaign', admin.RelatedOnlyFieldListFilter)
except Exception, e:
   _campaign_filter = 'campaign'


class CommunicationAdmin(admin.ModelAdmin):

   ordering = ['user', 'dt']

   date_hierarchy = 'dt'

   list_display = (
      'dt', 'user', 'campaign', 'direction', 'medium', 'status',
      'opens', 'clicks'
   )

   list_filter = (
      'direction', 'status', 'medium',
      _campaign_filter,
   )

   search_fields = ('user__email', )


def register_all():
   s = admin.site

   s.register(Template)
   s.register(Campaign, CampaignAdmin)
   s.register((Direction, Medium, MessageStatus,))
   s.register(Communication, CommunicationAdmin)


def register_event_foreign_models():
   s = admin.site

   for m in event_foreign_models:
      s.register(
         m,
         list_display=('name',),
         search_fields=('name',),
      )


