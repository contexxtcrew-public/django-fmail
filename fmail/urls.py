from fmail.views import UnsubscribeView

from django.conf.urls import patterns, url


urlpatterns = patterns('',
   url(r'^unsub/$', UnsubscribeView.as_view(), name='unsub'),
)

