import os
from setuptools import setup


with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
   name='django-fmail',
   version='0.1.1',
   packages=['fmail', 'fmail.migrations',],
   install_requires=['django>=1.7', 'django-migration-fixture>=0.5'],
   include_package_data=True,
   license='Proprietary',
   description='Django app that contains reusable email-related models and utilities',
   long_description=README,
   url='https://flyaps.com/',
   author='Alexander Arhipenko',
   author_email='aps@flyaps.com',
   classifiers=[
      'Environment :: Web Environment',
      'Framework :: Django',
      'Intended Audience :: Developers',
      'License :: Other/Proprietary License',
      'Operating System :: OS Independent',
      'Programming Language :: Python',
      'Programming Language :: Python :: 2.7',
      'Topic :: Internet :: WWW/HTTP',
      'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
   ],
)

